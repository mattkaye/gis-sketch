const $$ = Dom7;
const globals = require('./global');
const assert = require('./assert');
const utilities = require('./utility');
const mapObject = require('./mapObject');
const geolib = require('../../node_modules/geolib');

L.Control.Measure = L.Control.extend({
	onAdd(map) {
		var container = document.getElementById('toolbar');
		this._createButton(container, this._toggleMeasure, this);
		return container;
	},

	_createButton(container, fn, context) {
		var refernceNode = document.querySelector('[data-tool-triangle]'),
			newNode = L.DomUtil.create('a', '', container),
			that = this;
			
			$$(newNode).insertAfter(refernceNode);
			newNode.dataset.toolDistance = 1;

		L.DomEvent
			.on(newNode, 'click', L.DomEvent.stopPropagation)
			.on(newNode, 'click', L.DomEvent.preventDefault)
			.on(newNode, 'touchstart', fn, context)
			.on(newNode, 'dblclick', L.DomEvent.stopPropagation);

		return newNode;
	},

	_toggleMeasure(event) {
		if ($$('[data-tool-distance]').hasClass('active')) { 
			return; 
		}

        if (assert.distanceLinesPresent()) { 
        	this._measuring = true;
        	this._startMeasuring();
        	return; 
        }

		this._measuring = !this._measuring;
		this._measuring ? this._startMeasuring() : this._stopMeasuring();
	},

	_startMeasuring() {
		var that = this;
		this._lastClick = 0;
		
		if (!this._layerPaint) {
			this._layerPaint = L.layerGroup().addTo(this._map);

			// We use this to keep track of all parts of this distance line. Tooltips, lines, deletion boxes
			this._uniqueID = utilities.makeUniqueID();
		}

		if (!this._points) {
			this._points = [];
		}

		document.addEventListener('click', function(event) {
			that._mouseClick(event);
		});
	},

	_stopMeasuring() {
		L.DomEvent
			.off(this._map, 'click', this._mouseClick, this)
			.off(this._map, 'touchstart', this._mouseClick, this)
			.off(this._map, 'dblclick', this._mouseClick, this);

		this._restartPath();
	},

	_skipMouseClickEvent(event) {
		if (!event) {
			return true;
		}

		if (this._isPhantomClick()) {
			return true;
		}

		if (event.target.className.indexOf('leaflet-container') === -1) {
			this._finishPath();
			return true;
		}

		if (globals.get('activeMapTool') !== 'toolDistance') {
			return true;
		}

		if (!globals.get('map').mouseEventToLatLng(event) || Object.keys(event.target.dataset).length) {
			return true;
		}

		return false;
	},

	_mouseClick(event) {
		// Many reasons to bail on this method!
		if (this._skipMouseClickEvent(event)) {
			return;
		}

		var latlng = globals.get('map').mouseEventToLatLng(event);

		// If we have a tooltip, update the distance and create a new tooltip, leaving the old one exactly where it is (i.e. where the user has clicked)
		if (this._lastPoint && this._tooltip) {
			if (!this._distance) {
				this._distance = 0;
			}

			this._updateTooltipPosition(latlng);

			var distance = latlng.distanceTo(this._lastPoint);
			this._updateTooltipDistance(this._distance + distance, distance);

			this._distance += distance;
		}
		this._createTooltip(latlng);

		// If this is already the second click, add the location to the fix path (create one first if we don't have one)
		if (this._lastPoint && !this._layerPaintPath) {
			this._layerPaintPath = L.polyline([this._lastPoint], { 
				color: 'green',
				weight: 3,
				clickable: true,
				dashArray: '10,10',
				className: this._uniqueID
			}).addTo(this._layerPaint);
		}

		if (this._layerPaintPath) {
			this._layerPaintPath.addLatLng(latlng);
		}

		// Upate the end marker to the current location
		if (this._lastCircle) {
			this._layerPaint.removeLayer(this._lastCircle);
		}

		this._lastCircle = new L.CircleMarker(latlng, { 
			color: 'black', 
			opacity: 1, 
			weight: 1, 
			fill: true, 
			fillOpacity: 1,
			radius: 10,
			clickable: this._lastCircle ? true : false
		}).addTo(this._layerPaint);
		
		var that = this;
		$$(this._lastCircle._path.parentNode).on('click', function(event) {
			if (that._isPhantomClick()) { return; }
			that._finishPath();
		});

		// Save current location as last location
		this._lastPoint = latlng;
	},

	_finishPath() {
		$$("svg").appendTo(".dom");
		// Remove the last end marker as well as the last (moving tooltip)
		if (this._lastCircle) {
			this._layerPaint.removeLayer(this._lastCircle);
		}

		if (this._tooltip) {
			this._layerPaint.removeLayer(this._tooltip);
		}

		if (this._layerPaint && this._layerPaintPathTemp) {
			this._layerPaint.removeLayer(this._layerPaintPathTemp);
		}

		// Reset everything
		this._restartPath();
	},

	_restartPath() {
		this._distance = 0;
		this._tooltip = undefined;
		this._lastCircle = undefined;
		this._lastPoint = undefined;
		this._layerPaintPath = undefined;
		this._layerPaintPathTemp = undefined;
		this._uniqueID = utilities.makeUniqueID();
	},

	_createTooltip(position) {
		var icon = L.divIcon({
			className: 'leaflet-measure-tooltip ' + this._uniqueID,
			iconAnchor: [-5, -5]
		});
		this._tooltip = L.marker(position, { 
			icon: icon,
			clickable: false
		}).addTo(this._layerPaint);

		// Hide by default and only show if it has content
		this._tooltip._icon.style.display = 'none';
		this._showNonEmptyTooltips();
	},

	_showNonEmptyTooltips() {
		var allTooltips = document.querySelectorAll('.leaflet-measure-tooltip');
		for (var i = 0; i < allTooltips.length; i++) {
			if (allTooltips[i].innerHTML !== '') {
				allTooltips[i].style.display = 'block';
			}
		}
	},

	_updateTooltipPosition(position) {
		this._tooltip.setLatLng(position);
	},

	_updateTooltipDistance(total, difference) {
		var totalRound = roundNumber(total),
			differenceRound = roundNumber(difference),
			text = '';

		var displayedDistance = totalRound ? formatNumber(totalRound) : false;

		if (displayedDistance) {
			text = '<div class="leaflet-measure-tooltip-total">' + displayedDistance + ' ft</div>';
			if (differenceRound > 0 && totalRound != differenceRound) {
				text += '<div class="leaflet-measure-tooltip-difference">(+' + formatNumber(differenceRound) + ' ft)</div>';
			}
		}
		
		// Round to feet
		function roundNumber(value) {
			return Math.round(value / 0.3048);
		}

		function formatNumber(value) {
			return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		}

		this._tooltip._icon.innerHTML = text;
	},

	// Used to bypass odd double tooltip behavior
	_isPhantomClick() {
		var shouldSkip = false;
		var d = new Date();
        var t = d.getTime();
        if (t - this._lastClick < 100) {
            shouldSkip = true;
        }
        this._lastClick = t;
        return shouldSkip;
	}
});

L.Map.mergeOptions({
	measureControl: false
});

L.Map.addInitHook(function () {
	if (this.options.measureControl) {
		this.measureControl = new L.Control.Measure();
		this.addControl(this.measureControl);
	}
});

L.control.measure = function (options) {
	return new L.Control.Measure(options);
};