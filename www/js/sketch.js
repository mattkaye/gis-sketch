const $$ = Dom7;
const globals = require('./global');
const dom = require('./domObject');

// Quick hack for desired textbox behavior. Barf.
var textActive = false;

const sketch = {
    init() {
        var sketchMethods = Object.entries(this).filter(function(el) {
            return ['init', 'handleSpecialTool', 'addTextToCanvas', 'addFreehandDrawToCanvas'].indexOf(el[0]) === -1;
        });
        for (var i = 0; i < sketchMethods.length; i++) {
            sketchMethods[i][1]();
        }
    },

    createSketch() {
        var canvas = new fabric.Canvas("canvas-sketch", {
            selection: true,
            width: $$('.canvas-wrapper').outerWidth(),
            height: 600
        });

        globals.set('canvas', canvas);

        // Set first tool as active
        globals.set('activeSketchTool', 'toolNorth');

        // Add default handles for all objects
        fabric.Object.prototype.set({
            padding: 20,
            cornerSize: 30,
            cornerStyle: 'circle'
        });
    },

    setCanvasBackgroud() {
        var canvas = globals.get('canvas');
        var center = canvas.getCenter();

        canvas.setBackgroundImage(globals.get('fileSystemPath') + '/../images/sketch-background.png', canvas.renderAll.bind(canvas), {
            width: canvas.width,
            height: canvas.height,
            originX: 'left',
            originY: 'top'
        });
    },

    bindToolSelectionHandler() {
        var toolbarButtons = $$('#toolbar-sketch a');

        $$(toolbarButtons).on('click', function() {
            $$(toolbarButtons).removeClass('active');
            $$(this).addClass('active');
            globals.set('activeSketchTool', Object.keys(this.dataset)[0]);

            if (globals.get('activeSketchTool') === 'toolDraw') {
                sketch.addFreehandDrawToCanvas();
            } else {
                sketch.removeFreehandDrawFromCanvas();
            }

        });
    },

    addRemoveShapeToCanvas() {
        var canvas = globals.get('canvas');
        var specialTools = ['toolDraw', 'toolText'];

        canvas.on('mouse:down', function(options) {
            if (textActive) {
                textActive = false; 
                return; 
            } 

            // Delete this object
            if (globals.get('activeSketchTool') === 'toolDelete') {
                canvas.remove(canvas.getActiveObject());
                return;

            // Stop making double objects when one is being clicked
            } else if (canvas.getActiveObject() && globals.get('activeSketchTool') !== 'toolDelete') {
                return;
            }

            if (options.e.type == 'touchstart') {
                var event = options.e.touches ? options.e.touches[0] : options.e;
                var event_offset = canvas.getPointer(event);

                // Handle special cases
                if (specialTools.indexOf(globals.get('activeSketchTool')) > -1) {
                    sketch.handleSpecialTool(globals.get('activeSketchTool'), event_offset);
                    return;
                }

                // Add image to canvas
                var img = fabric.Image.fromURL(globals.get('fileSystemPath') + '/../images/tools/sketch/' + globals.get('activeSketchTool') + '.svg', function(img) {
                    img.set({
                        left: event_offset.x,
                        top: event_offset.y
                    });
                    canvas.add(img).setActiveObject(img);
                });
            }
        });
    },

    handleSpecialTool(toolName, event_offset) {
        var activeObject = globals.get('canvas').getActiveObject();
        switch(toolName) {
            case 'toolText':
                this.addTextToCanvas(event_offset);
                break;

            case 'toolDraw':
                this.addFreehandDrawToCanvas();
                break;
        }
    },

    addTextToCanvas(event_offset) {
        var canvas = globals.get('canvas');
        var textBox = new fabric.IText('', {
            left: event_offset.x,
            top: event_offset.y,
            fontSize: 20,
            fontFamily: 'Helvetica'
        });
        canvas.add(textBox);
        canvas.setActiveObject(textBox);
        textBox.enterEditing();
        textBox.hiddenTextarea.focus();
    },

    addFreehandDrawToCanvas() {
        globals.get('canvas').isDrawingMode = 1;
    },

    removeFreehandDrawFromCanvas() {
        globals.get('canvas').isDrawingMode = 0;
    },

    setDrawingAsActiveObject() {
        var canvas = globals.get('canvas');
        canvas.on('path:created', function(event) {
            if (event.path.height < 1) {
                canvas.remove(event.path);
            }
            var lastDrawing = canvas.getObjects()[canvas.getObjects().length - 1];
            if (lastDrawing) {
                canvas.setActiveObject(lastDrawing);
                sketch.removeFreehandDrawFromCanvas();
            }
        });
    },

    setTextAsActiveObject() {
        var canvas = globals.get('canvas');
        canvas.on('text:editing:exited', function(event) {
            var lastTextbox = canvas.getObjects()[canvas.getObjects().length - 1];
            if (lastTextbox.text.length) {
                textActive = true;
                canvas.setActiveObject(lastTextbox);
            } else {
                canvas.remove(lastTextbox);
            }
        });
    }
};

module.exports = sketch;