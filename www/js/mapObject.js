const $$ = Dom7;
const globals = require('./global');
const assert = require('./assert');
const turfBufferLine = require('../../node_modules/@turf/buffer');

const mapObjects = { 
    removeMarker(marker) {
        var map = globals.get('map');
        // Marker ID's not matching for some reason, use zIndex since they're unique
        var foundMarker = this.getMatchingMarkerByAttribute(marker.style.zIndex);
        foundMarker.removeFrom(map);
    },

    addMarker(mouseEvent) {
        var map = globals.get('map');
        var this_icon = L.icon({
            iconUrl: globals.get('fileSystemPath') + '/../images/tools/map/' + globals.get('activeMapTool') + '.svg',
        });

        var coordinates = Object.values(map.mouseEventToLatLng(mouseEvent));

        return L.marker(coordinates, {
            draggable: true,
            icon: this_icon
        }).addTo(map);
    },

    removeDistanceLine(uniqueIDs) {
        // Remove all objects with unqiue ID classname
        var map = globals.get('map');
        $$(uniqueIDs.join(',')).on('click', function() {
            var clickedNodeClassName = $$(this)[0].classList[0];

            map.eachLayer(function(layer) {
                if (layer.options && layer.options.className && layer.options.className === clickedNodeClassName) {
                    layer.removeFrom(map);
                    $$('.' + clickedNodeClassName).remove();
                }
            });

        });
    },

    getMatchingMarkerByAttribute(zIndex) {
        var map = globals.get('map'),
            foundMarker;

        map.eachLayer(function(layer) { 
            // It's a marker layer with the matching zIndex
            if (layer._zIndex == zIndex) {
                foundMarker = layer;
            }
        });

        return foundMarker;
    },

    drawDistanceDeletionBoxes() {
        var map = globals.get('map');
        this.clearPreviousDistanceDeletionBoxes();
        var allLineBounds = this.getAllDistanceLines();

        if (allLineBounds.length) {
            var uniqueIDs = [];
            for (var i = 0; i < allLineBounds.length; i++) {
                var thisBoundingBox = L.rectangle(allLineBounds[i]._latlngs, {
                    color: globals.get('boundingBoxColor'), 
                    weight: 1,
                    className: allLineBounds[i].options.className
                }).addTo(map);
                uniqueIDs.push('.' + allLineBounds[i].options.className);
            }
            mapObjects.removeDistanceLine(uniqueIDs); 
        }
    },

    clearPreviousDistanceDeletionBoxes() {
        map = globals.get('map');
        map.eachLayer(function(layer) {
            if (layer.options.color === globals.get('boundingBoxColor')) {
                layer.removeFrom(map);
            }
        });
    },

    getAllDistanceLines() {
        var map = globals.get('map'),
            foundDistanceLayers = [];

        map.eachLayer(function(layer) { 
            if (layer._latlngs && layer.options.className !== 'mainline buffer') {
                foundDistanceLayers.push(layer);
            }
        });
        console.log(foundDistanceLayers);
        return foundDistanceLayers;
    },

    toggleMainLine(bool) {
        var map = globals.get('map');
        if (bool) {
            // Marcus Blvd
            var mainLineLngLat = [
                [40.808888, -73.246869],
                [40.810293, -73.246955],
                [40.811, -73.247008],
                [40.811024, -73.246933],
                [40.811032, -73.246622],
                [40.811048, -73.246284],
                [40.811032, -73.245903],
                [40.810996, -73.245431],
                [40.810971, -73.245088],
                [40.810927, -73.24475]
            ];

            // Flip lat lng for Leaflet
            mainLineLngLat = mainLineLngLat.map(function(el) {
                return el.reverse();
            });

            var mainLine = {
              type: 'Feature',
              geometry: {
                type: 'LineString',
                coordinates: mainLineLngLat
              }
            };

            // Add main line
            var line = L.geoJSON(mainLine, {
                color: globals.get('colors').gas,
                weight: 5,
                className: 'mainline'
            }).addTo(map);

            // Add buffer around main line
            var polygon = turfBufferLine(mainLine, 50, {units: 'feet'});
            L.geoJSON(polygon, {
                color: 'green',
                fill: false,
                opacity: 0.25,
                className: 'mainline buffer'
            }).addTo(map);

            map.fitBounds(line.getBounds());

        } else {
             map.eachLayer(function(layer) {
                if (layer.options && layer.options.className && layer.options.className === 'mainline') {
                    layer.removeFrom(map);
                }
            });
        }
    },

    zoomBoundsToFitObjects() {
        var map = globals.get('map'),
            allMarkers = [];

        map.eachLayer(function(layer) { 
            if (layer._latlng || layer._latlngs) {
                allMarkers.push(layer);
            }
        });

        if (!allMarkers.length) {
            return false;
        }

        map.fitBounds(new L.featureGroup(allMarkers).getBounds().pad(.25));
        return true;
    },

    saveMarkerStates() {
        var map = globals.get('map'),
            allMarkers = [];

        map.eachLayer(function (layer) { 
            if (layer instanceof L.Marker) {
                allMarkers.push(layer);
            }
        });

        if (!allMarkers.length) {
            return false;
        } else {
            globals.set('markers', allMarkers);
        }
    },

    backOneStep() {
        var map = globals.get('map'),
            markers = globals.get('markers');
        
        if (!markers.length) {
            return;
        }

        // Last marker added
        var lastMarker = markers[markers.length - 1];

        if (lastMarker.type === 'layeradd') {
            lastMarker.layer.removeFrom(map);
        } else if (lastMarker.type === 'layerremove') {
            lastMarker.layer.addTo(map);
        } else {
            // Some other state yet to be handled
        }
    },

    clearMap() {
        var map = globals.get('map');

        map.eachLayer(function (layer) { 
            try {
                if (layer.toGeoJSON()) {
                    layer.removeFrom(map);
                }
            } catch (e) {}
        });
        globals.set('markers', []);
    },

    exportMapAsImage() {
        var printer = L.easyPrint({
            filename: 'exported-map',
            exportOnly: true,
            hideControlContainer: true,
            hidden: true
        }).addTo(globals.get('map'));

        if (assert.isContinuingMainMeasurement()) {
            this.saveMapState();
        }

        // Adjust the map div height to match parent dimension in absolute pixels
        $$('#map').css('height', $$('.map-page .content-block').outerHeight() + 'px');
        printer.printMap('CurrentSize', 'map-snapshot');
        
        // Set it back to vh parameter
        $$('#map').css('height', '100vh');
    },

    saveMapState() {
        var map = globals.get('map');
        var collection = {
            "type": "FeatureCollection",
            "features": []
        };
        map.eachLayer(function (layer) {
            try {
                // It's a JSON feature, or catch the exception and exit
                let featureType = layer.toGeoJSON().type;

                // Only one FeatureCollection allowed
                if (featureType === 'FeatureCollection') { 
                    return; 
                }

                collection.features.push(layer.toGeoJSON());
                
                var lastAddedFeature = collection.features[collection.features.length - 1];

                // Store main line measuing buffer and main line styles
                if (layer.defaultOptions) {
                    lastAddedFeature.properties = lastAddedFeature.properties || {};
                    lastAddedFeature.properties.styles = layer.defaultOptions;
                    return;
                }

                // Store class for measure line
                if (layer.options.className) {
                    lastAddedFeature.properties.className = layer.options.className;
                    return;
                }

                // Store custom markers file name
                if (layer.options.icon.options.iconUrl) {
                    let iconPathParts = layer.options.icon.options.iconUrl.split('/');
                    let iconFile = iconPathParts[iconPathParts.length - 1];
                    lastAddedFeature.properties.iconName = iconFile;
                    return;
                }

                // Store measure tooltip distance class
                if (layer.options.icon && layer.options.icon.options.className.indexOf('leaflet-measure-tooltip') !== -1) {
                    lastAddedFeature.properties.className = layer.options.icon.options.className;
                    // Store markup
                    lastAddedFeature.properties.toolTip = layer._icon.innerHTML;
                    // Store the footage measured this time
                    if (assert.isContinuingMainMeasurement()) {
                        lastAddedFeature.properties.footage = lastAddedFeature.properties.toolTip.match(/\d+/)[0];
                    }
                    return;
                }
                
            } catch (e) {console.log(e);} 
        });
        globals.set('mapState', collection);
    },

    restoreMapState() {
        var map = globals.get('map');

        L.geoJSON(globals.get('mapState'), {
            pointToLayer: (feature, latlng) => {
                // Custom marker
                if (feature.properties.iconName) {
                    var customMarker = L.icon({
                        iconUrl: globals.get('fileSystemPath') + '/../images/tools/map/' + feature.properties.iconName,
                    });
                    return L.marker(latlng, {
                        draggable: true,
                        icon: customMarker
                    });
                } else {
                    // Tooltip for measuring
                    // Skip empty tooltips
                    if (feature.properties.toolTip) {
                        var distanceIcon = L.divIcon({ 
                            iconSize: new L.Point(latlng), 
                            iconAnchor: [-5, -5],
                            className: feature.properties.className,
                            html: feature.properties.toolTip
                        });
                        return L.marker(latlng, {
                            icon: distanceIcon
                        });
                    }
                    
                }
            },
            style: (feature) => {
                // Main line buffer and main line
                if (feature.properties.styles) {
                    return feature.properties.styles;
                }

                // Standard measurement
                return {
                    color: 'green',
                    weight: 3,
                    clickable: true,
                    dashArray: '10,10',
                    className: feature.properties.className
                }

                return feature.properties.styles;
            }
        }).addTo(map);
        this.zoomBoundsToFitObjects();
    }
};

$$('.logo').on('click', () => mapObjects.saveMapState());

$$('.toolbar-bottom').on('click', () => mapObjects.restoreMapState());

module.exports = mapObjects;