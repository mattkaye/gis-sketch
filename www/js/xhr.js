const $$ = Dom7;
const globals = require('./global');
const utilities = require('./utility');
const assert = require('./assert');

const xhr = {
    postToFulcrum(sketchApp, whichImage, recordID, formID) {
        // Remove handlebars from active objects so they don't transfer to the JPG
        if (whichImage === 'Sketch') {
            utilities.discardActiveSketchObjects();
        }

        $$.ajax({
        	url: globals.get('uploadEndPoint'),
            method: 'POST',
            dataType: 'json',
            data: {
                base64: whichImage === 'Sketch' ? utilities.canvasToBase64('#ffffff', 'canvas-sketch') : globals.get('map').encodedMap,
                recordID : recordID,
                formID: formID,
                type: whichImage,
                geoJSON: assert.isContinuingMainMeasurement() ? globals.get('mapState') : false
            },
            success: function(response) {
                console.log(response);
                sketchApp.hidePreloader();
                sketchApp.alert('Upload complete! Press the back button to return to Fulcrum.', globals.get('appName'));
            },
            error: function(xhr, status) {
                console.log('Error: ' + JSON.stringify(xhr));
                console.log('ErrorStatus: ' + JSON.stringify(status));
                sketchApp.hidePreloader();
            }
        });
    }
};

module.exports = xhr;