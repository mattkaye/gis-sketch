const $$ = Dom7;
const globals = require('./global');
const utilities = require('./utility');
const assert = require('./assert');
const mapObjects = require('./mapObject');
const sketch = require('./sketch');
const xhr = require('./xhr');

const domObjects = { 
    init(sketchApp) { 
        let domMethods = Object.entries(this).filter(function(el) {
            return el[0] !== 'init'; 
        });
        for (var i = 0; i < domMethods.length; i++) { 
            domMethods[i][1](sketchApp); 
        }
    },
    
    flyToLocationHandler() {
        $$('[data-tool-location]').on('click', function() {
            utilities.flyToLocation(globals.get('latLng'), 15); 
        });
    },
   
    onUndoHandler() { 
        $$('[data-tool-undo]').on('click', function() {
            mapObjects.backOneStep();
        });
    },
 
    bindToolSelectionHandler() {  
        var toolbarButtons = $$('#toolbar a');

        $$(toolbarButtons).on('click', function() {
            $$(toolbarButtons).removeClass('active');
            $$(this).addClass('active');

            globals.set('activeMapTool', Object.keys(this.dataset)[0]);

            if (['toolDelete', 'toolUndo'].indexOf(globals.get('activeMapTool')) === -1) {
                mapObjects.clearPreviousDistanceDeletionBoxes();
            }
        });
    },
    
    bindTabChange() {
        $$('.toolbar-inner .map').on('click', function() {
            // Reset the map view. Viewport may have changed
            setTimeout(() => {
                globals.get('map').invalidateSize();
            }, 100);
        });

        $$('.toolbar-inner .sketch').on('click', function() {
            // Reset the map view. Viewport may have changed
            setTimeout(() => {
                globals.get('canvas').setWidth($$('.canvas-wrapper').outerWidth());
                sketch.setCanvasBackgroud();
            }, 100);
        });
    },

    bindMarkerObjectsHandler() {
        // map.on('click') event fires inconsistently on mobile devices
        var nonMarkerTools = ['toolLocation', 'toolDelete', 'toolUndo', 'toolMarkerBounds'];
        document.getElementById('map').addEventListener('click', function(event) {
            var activeMapTool = globals.get('activeMapTool');
            
            if (activeMapTool === 'toolDistance') { return; }

            if (activeMapTool === 'toolDelete' && assert.toolbarWasClicked(event.target)) {
                mapObjects.drawDistanceDeletionBoxes();
            }

            var doubleTapTimeout = setTimeout(function() {
                // Double tap detected. Don't add or remove any markers
                if (assert.isDoubleTap()) { return; }

                // Event not triggered from map or a tool has not been selecated
                if (assert.toolbarWasClicked(event.target) || !activeMapTool) {
                    return;
                }

                // Add the marker
                if (nonMarkerTools.indexOf(activeMapTool) === -1) {
                    mapObjects.addMarker(event);
                }

                // Remove the marker
                if (assert.willDeleteMarker(event.target)) {
                    mapObjects.removeMarker(event.target);
                }
            }, 300);
            
        });
    },

    doubleTapHandler() { 
        var map = globals.get('map');
        document.getElementById('map').addEventListener('touchstart', tapHandler);
 
        var tapedTwice = false;
  
        function tapHandler(event) {
            if(!tapedTwice) { 
                tapedTwice = true;
                setTimeout( function() { tapedTwice = false; }, 300 );
                globals.set('doubleTap', false);
                return false;
            }
            event.preventDefault();
            map.setZoom(map.getZoom() + 1);
            globals.set('doubleTap', true);
        } 
    }, 

    zoomToBoundsHandler(sketchApp) {
        $$('[data-tool-marker-bounds]').on('click', function () {
            if (!mapObjects.zoomBoundsToFitObjects()) {
                sketchApp.alert('There are no markers on the map.', globals.get('appName'));
            }
        });
    },

    uploadHandler(sketchApp) {
        $$('[data-speeddial-upload]').on('click', function () {
            sketchApp.showPreloader('One Moment...');
            var recordID = assert.hasFulcrumRecordID();
            var formID = assert.hasFulcrumFormID();
            var whichImage = utilities.whichSpeedDial(this);
            if (recordID && formID) {
                whichImage === 'Sketch' ? xhr.postToFulcrum(sketchApp, whichImage, recordID, formID) : mapObjects.exportMapAsImage();
            } else {
                sketchApp.hidePreloader();
                sketchApp.alert('You must update a record from the Fulcrum app to upload a sketch or map.', globals.get('appName'));
            }
        }); 
    },

    resetHandler(sketchApp) {
        $$('[data-speeddial-reset]').on('click', function () {
            var whichSpeedDial = utilities.whichSpeedDial(this);
            sketchApp.confirm('Are you sure you want to reset the ' + whichSpeedDial.toLowerCase() + '? This action cannot be undone.', globals.get('appName'), function () {

                if (whichSpeedDial === 'Map') {
                    mapObjects.clearMap();
                } else {
                    var canvas = globals.get('canvas');
                    var canvasObjects = [];
                    canvas.forEachObject(function (o) {
                        canvasObjects.push(o);
                    });

                    for (var i = 0; i < canvasObjects.length; i++) {
                        canvas.remove(canvasObjects[i]);
                    }
                }
                
                sketchApp.alert(whichSpeedDial + ' reset!', globals.get('appName'));
            });
        });
    },

    onAddRemoveLayerHandler() {
        var map = globals.get('map')
            actions = ['layeradd', 'layerremove'];

        for (let i = 0; i < actions.length; i++) {
            map.on(actions[i], function (layer) {
                if (layer instanceof L.Marker) {
                    globals.set('markers', layer);
                }
            });
        }
    },

    orientationChanged() {
        // Wait until innerheight changes, for max 10 frames
        const timeout = 10;
        return new window.Promise(function(resolve) {
            const go = (i, height0) => {
                window.innerHeight != height0 || i >= timeout ?
                    resolve() :
                    window.requestAnimationFrame(() => go(i + 1, height0));
            };
            go(0, window.innerHeight);
        });
    },

    orientationchangeHandler() {
        window.addEventListener('orientationchange', function() {
            domObjects.orientationChanged().then(function() {
                globals.get('canvas').setWidth($$('.canvas-wrapper').outerWidth());
                sketch.setCanvasBackgroud();
            });
        });
    },

    mapPrintHandler(sketchApp) {
        globals.get('map').on('easyPrint-finished', function() {
            xhr.postToFulcrum(sketchApp, 'Map', globals.get('fulcrumRecordData').recordID, globals.get('fulcrumRecordData').formID);
        });
    },

    mapPopupHandler(sketchApp) {
        $$('.speed-dial.map-layers').on('click', function () {
          var popupHTML = `
              <div class="popup">
                <div class="content-block">
                  <h3>Add Layers</h3>
                  <div class="item-content">
                    <div class="item-inner">
                      <div class="item-input">
                        <table>
                            <tr>
                                <td>
                                    <label class="label-switch">
                                      <input type="checkbox">
                                      <div class="checkbox"></div>
                                    </label>
                                </td>
                                <td>
                                    <p>Gas Main</p>
                                </td>
                            </tr>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>`;
          sketchApp.popup(popupHTML);
          domObjects.mapLayerToggleHandler();
        });
    },

    mapLayerToggleHandler() {
        var map = globals.get('map');
        $$('input[type="checkbox"]').on('change', function () {
            mapObjects.toggleMainLine($$(this).prop('checked'));
        });
    }
};

module.exports = domObjects;