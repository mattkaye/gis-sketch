const globals = {
    setDefaults() { 
        Template7.global = {
            appName: '/sk≡tch/',
            colors: {
                proposedEvacuation: 'white', 
                temporarySurveyMarkings: 'pink', 
                gas: 'yellow',
                oil: 'yellow', 
                steam: 'yellow', 
                communication: 'orange',
                water: 'blue',
                reclaimedWater: 'purple',
                sewer: 'green'
            },
            map: false,
            mapState: false,
            markers: [],
            latLng: {}, 
            zoom: 9,
            activeMapTool: false,
            activeSketchTool: false,
            boundingBoxColor: 'red',
            fileSystemPath: window.rootFS,
            canvas: false,
            doubleTap: false,
            uploadEndPoint: 'http://ec2-34-229-45-56.compute-1.amazonaws.com/phonegap-sketch/tabbed-layout/update-fulcrum-record.php',
            fulcrumRecordData: false,
            mapboxKey: 'pk.eyJ1IjoibWtzZXF1ZWwiLCJhIjoiY2o4dnU3eDdqMDBrdjJ4cGpiMm5yYmMwdCJ9.MhlrrGrSQuxSL29ywHcmvw'
        };
    },

    set(key, value) {
        if (Array.isArray(Template7.global[key])) {
            Template7.global[key].push(value);
        } else {
            Template7.global[key] = value;
        }
    },

    get(key) {
        return Template7.global[key];
    }
};

module.exports = globals;