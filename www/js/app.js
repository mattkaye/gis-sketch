const sketchApp = new Framework7();
const $$ = Dom7; 

require('../../node_modules/leaflet');
require('./leafletMeasure');
require('./leafletPrint');
const globals = require('./global'); 
const utilities = require('./utility');
const dom = require('./domObject');
const sketch = require('./sketch');

$$(document).on('deviceready', function() {
	globals.setDefaults();

	var map = L.map('map', {
	    center: [40.993902, -73.4796498],
	    measureControl: true,
	    zoom: globals.get('zoom')
	}); 

	// Adding base map layer
	L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=' + globals.get('mapboxKey'), {
	    id: 'mapbox.streets'
	}).addTo(map);

	// Add scale
	L.control.scale({
		position: 'topright' 
	}).addTo(map);

	globals.set('map', map);

	utilities.init();
	sketch.init();
	dom.init(sketchApp);

	// Log shortcut saves dev time
	window.c = console.log.bind(console);
});