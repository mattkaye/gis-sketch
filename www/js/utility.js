const $$ = Dom7;
const globals = require('./global');
const assert = require('./assert');
const mapObjects = require('./mapObject');
const geolib = require('../../node_modules/geolib');

const utilities = {
    init() {
    	let utilityMethods = Object.entries(this).filter(function(el) {
            return ['init','flyToLocation', 'whichSpeedDial', 'canvasToBase64', 'discardActiveSketchObjects'].indexOf(el[0]) === -1;
        });
        for (var i = 0; i < utilityMethods.length; i++) {
            utilityMethods[i][1]();
        } 
    },

    skinApp() {
        if (assert.isAndroid()) {
            $$('.logo img').prop('src', 'images/logo-android.png');
            Dom7('head').append(
                '<link rel="stylesheet" href="lib/framework7/css/framework7.material.min.css">' +
                '<link rel="stylesheet" href="lib/framework7/css/framework7.material.colors.min.css">' +
                '<link rel="stylesheet" href="css/material.css">'
            );
        } else {
            $$('.logo img').prop('src', 'images/logo-ios.png');
            Dom7('head').append(
                '<link rel="stylesheet" href="lib/framework7/css/framework7.ios.min.css">' +
                '<link rel="stylesheet" href="lib/framework7/css/framework7.ios.colors.min.css">' +
                '<link rel="stylesheet" href="css/ios.css">'
            );
        }
    },

    selectActiveTab() {
        setTimeout(function() {
            var fulcrumData = globals.get('fulcrumRecordData');
            if (!fulcrumData) { return; }

            // Continuation of mainline measurement
            if (assert.isContinuingMainMeasurement()) {
                mapObjects.toggleMainLine(true);
                return;
            }

            // Change tab to sketch
            if (fulcrumData.eventField === 'leak_sketch_required') {
                $$('#view-map').removeClass('active');
                $$('.tab-link.map').removeClass('active');

                $$('#view-sketch').addClass('active');
                $$('.tab-link.sketch').addClass('active');
            }
        }, 1000);
    },

    getLocation() {
        navigator.geolocation.getCurrentPosition(success, error);

        function success(position) {
            globals.set('latLng', {lat: position.coords.latitude, lng: position.coords.longitude});
        };

        function error(err) {
            console.warn(`ERROR(${err.code}): ${err.message}`);
        };
    },

    flyToLocation(latLng, zoomLevel) {
    	globals.get('map').flyTo(latLng, zoomLevel);
    },

    makeUniqueID(length) {
        var length = length || 8;
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

        for (var i = 0; i < length; i++) {
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        return text;
    },

    whichSpeedDial(DomNode) {
    	var whichSpeedDial = $$(DomNode).parents('.speed-dial-buttons').dataset();
       	return Object.keys(whichSpeedDial)[0] === 'typeMap' ? 'Map' : 'Sketch';
    },

    discardActiveSketchObjects() {
    	globals.get('canvas').discardActiveObject().discardActiveGroup().renderAll();
    },

    canvasToBase64(backgroundColor, canvasId){
	    var context = document.getElementById(canvasId).getContext('2d');
	    var temp_canvas = context.canvas;

	    //cache height and width        
	    var w = temp_canvas.width;
	    var h = temp_canvas.height;

	    var data;

	    //get the current ImageData for the canvas.
	    data = context.getImageData(0, 0, w, h);

	    //store the current globalCompositeOperation
	    var compositeOperation = context.globalCompositeOperation;

	    //set to draw behind current content
	    context.globalCompositeOperation = "destination-over";

	    //set background color
	    context.fillStyle = backgroundColor;

	    //draw background / rect on entire canvas
	    context.fillRect(0, 0, w, h);

	    //get the image data from the canvas
	    var imageData = temp_canvas.toDataURL("image/jpeg");

	    //clear the canvas
	    context.clearRect (0, 0, w, h);

	    //restore it with original / cached ImageData
	    context.putImageData(data, 0, 0);

	    //reset the globalCompositeOperation to what it was
	    context.globalCompositeOperation = compositeOperation;

	    //return the Base64 encoded data url string
	    return imageData;
	}
};

module.exports = utilities;