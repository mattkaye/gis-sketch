const $$ = Dom7;
const globals = require('./global');

const assert = {
    isAndroid() {
        return Framework7.prototype.device.android;
    },

    isIOS() {
        return Framework7.prototype.device.ios;
    },

    isDoubleTap() {
        return globals.get('doubleTap');
    }, 
    
    willDeleteMarker(domObject) {
        return globals.get('activeMapTool') === 'toolDelete' && $$(domObject).hasClass('leaflet-marker-icon');
    },

    toolbarWasClicked(eventTarget) {
        return $$(eventTarget).parent().attr('id') === 'toolbar';
    }, 

    distanceLinesPresent() {
        var distanceLinesPresent = false;
        globals.get('map').eachLayer(function (layer) { 
            if(layer._latlngs) {
                distanceLinesPresent = true;
            }
        });
        return distanceLinesPresent;
    },

    hasFulcrumRecordID() {
        return globals.get('fulcrumRecordData') ? globals.get('fulcrumRecordData').recordID : false;
    },

    hasFulcrumFormID() {
        return globals.get('fulcrumRecordData') ? globals.get('fulcrumRecordData').formID : false;
    },

    isContinuingMainMeasurement() {
        return Object.keys(globals.get('fulcrumRecordData')).indexOf('continueMeasurement') !== -1;
    }
};

module.exports = assert;