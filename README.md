# GIS Sketch

### For Android and iOS handheld devices

GIS Sketch is a GIS accurate mapping application tailored for the utility industry, most notably **Con Edison** and **National Grid**. This app was built to tie into the Fulcrum API. Records are collected at the point of meter inspection or dig site, and maps are generated as well as optional sketches of the dig site. See app functionality by clicking on the device previews below.

### Technology used to build:

 * [Phonegap](https://github.com/phonegap) - Cross device hybrid mobile app library.
 * [Framework7](https://github.com/framework7io/Framework7) - Full featured HTML framework for building iOS & Android apps.
 * [Leaflet](https://github.com/Leaflet/Leaflet) - Lightweight mapping library.
 * [Fabric.js](https://github.com/kangax/fabric.js/) - Javascript Canvas Library, SVG-to-Canvas (& canvas-to-SVG) Parser.
 * [Browserify](https://github.com/browserify/browserify) - Browser-side require() the nodejs way.
 * [Babel](https://github.com/babel/babel) - Babel is a compiler for writing next generation JavaScript.
 * [Fulcrum](http://www.fulcrumapp.com/) - A general purpose data point collection tool.

### Android Tablet
[![](http://i.imgur.com/BpyC1Os.png)](http://138.197.95.142/matt-portfolio/sketch/sketch-map.mp4)

### iPad
[![](http://i.imgur.com/pNH8d2e.png)](http://138.197.95.142/matt-portfolio/sketch/sketch-canvas.mp4)